%global gem_name activerecord
Name:                rubygem-%{gem_name}
Epoch:               1
Version:             7.0.7
Release:             1
Summary:             Object-relational mapper framework (part of Rails)
License:             MIT
URL:                 http://rubyonrails.org
Source0:             https://rubygems.org/gems/%{gem_name}-%{version}.gem
# The gem doesn't ship with the test suite.
# You may check it out like so
# git clone http://github.com/rails/rails.git
# cd rails/activerecord && git archive -v -o activerecord-7.0.7-tests.txz v7.0.7 test/
Source1:             activerecord-%{version}-tests.txz
# The tools are needed for the test suite, are however unpackaged in gem file.
# You may check it out like so
# git clone http://github.com/rails/rails.git --no-checkout
# cd rails && git archive -v -o rails-7.0.7-tools.txz v7.0.7 tools/
Source2:             rails-%{version}-tools.txz
# Fixes for Minitest 5.16+
# https://github.com/rails/rails/pull/45380
Patch1:              rubygem-activerecord-7.0.2.3-Remove-the-multi-call-form-of-assert_called_with.patch
# https://github.com/rails/rails/pull/45370
Patch2:              rubygem-activerecord-7.0.2.3-Fix-tests-for-minitest-5.16.patch
Patch3:              rubygem-activerecord-7.0.4-remove-require-pathname-from-drop-method.patch
Patch4:              rubygem-activerecord-7.0.4-remove-require-pathname-from-drop-method-tests.patch

Suggests:            %{_bindir}/sqlite3
BuildRequires:       rubygems-devel rubygem(bcrypt) rubygem(activesupport) = %{version}
BuildRequires:       rubygem(activemodel) = %{version} rubygem(builder) rubygem(sqlite3)
BuildRequires:       rubygem(actionpack) = %{version} rubygem(pg) rubygem(mocha) rubygem(rack)
BuildRequires:       rubygem(zeitwerk) rubygem(benchmark-ips) rubygem(did_you_mean)
BuildArch:           noarch
%description
Implements the ActiveRecord pattern (Fowler, PoEAA) for ORM. It ties database
tables and classes together for business objects, like Customer or
Subscription, that can find, save, and destroy themselves without resorting to
manual SQL.

%package doc
Summary:             Documentation for %{name}
Requires:            %{name} = %{epoch}:%{version}-%{release}
BuildArch:           noarch
%description doc
Documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version} -b1 -b2

%patch 3 -p2
 
pushd %{_builddir}
%patch 1 -p2
%patch 2 -p2
%patch 4 -p2
popd
%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%check
pushd .%{gem_instdir}
ln -s %{_builddir}/tools ..
mv %{_builddir}/test .
sed -i '/require .byebug./ s/^/#/g' test/cases/base_prevent_writes_test.rb
sed -i '/assert_slower_by_at_most/ s/1\.4/2.5/' \
  test/cases/encryption/performance/envelope_encryption_performance_test.rb
# Test adapters separately
mv -v test/cases/adapters/ %{_builddir}/.

# Run without adapters
ruby -Itest:lib -e '
  Dir.glob("./test/cases/**/*_test.rb")
    .sort
    .reject { |f| f =~ %r|/encryption/performance/| }
    .each { |f| require f }'

# Return the adapters to test them
mv -v %{_builddir}/adapters/ test/cases/.

# Run tests for adapters only, but without postgresql
ruby -Itest:lib -e '
  Dir.glob("./test/cases/adapters/**/*_test.rb")
    .sort
    .reject {|x| x =~ %r|/postgresql/| }
    .each { |f| require f }'
popd

%files
%dir %{gem_instdir}
%license %{gem_instdir}/MIT-LICENSE
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CHANGELOG.md
%doc %{gem_instdir}/README.rdoc
%{gem_instdir}/examples

%changelog
* Fri Aug 18 2023 liyanan <thistleslyn@163.com> - 1:7.0.7-1
- Upgrade to version 7.0.7

* Mon Aug 14 2023 liyanan <thistleslyn@163.com> - 1:7.0.4-3
- fix build error 

* Wed Feb 22 2023 wushaozheng <wushaozheng@ncti-gba.cn> - 1:7.0.4-2
- fix CVE-2022-44566 CVE-2023-22794
 
* Thu Jan 19 2023 yanxiaobing <yanxiaobing@huawei.com> - 1:7.0.4-1
- Upgrade to version 7.0.4

* Tue Mar 29 2022 liyanan <liyanan32@huawei.com> - 6.1.4.1-1
- update to 6.1.4.1

* Tue Mar 16 2021 wangyue <wangyue92@huawei.com> - 5.2.4.4-2
- fix CVE-2021-22880
 
* Mon Feb  8 2021 sunguoshuai<sunguoshuai@huawei.com>- 5.2.4.4-1
- Upgrade to 5.2.4.4

* Wed Aug 12 2020 chengzihan <chengzihan2@huawei.com> - 5.2.3-1
- Package init
